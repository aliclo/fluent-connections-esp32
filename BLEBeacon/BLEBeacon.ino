#include "BLEDevice.h"

#define BAUD_RATE 115200
#define SERVER_NAME "TEMP83726"
#define MODEL_NUMBER "43234"
#define SERIAL_NUMBER "712"

#define TEMP_PIN 35

#define THING_MAX_VOLTS 3.3
#define THING_MAX_ANALOG_VALUE 4095
#define TEMP_MAX_VOLTS 1.7

#define TEMP_MAX 125
#define TEMP_MIN -40
#define TEMP_RANGE (TEMP_MAX-TEMP_MIN)

//4095x = 3.3, where x is a value that converts from an analog reading to volts,
//x = 3.3/4095
#define ANALOG_TO_VOLTS (THING_MAX_VOLTS/THING_MAX_ANALOG_VALUE)
#define TEMP_MAX_ANALOG_VALUE (TEMP_MAX_VOLTS/ANALOG_TO_VOLTS)

#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARA_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

BLECharacteristic *chara;

/*#define ADDRESS_LENGTH 6

String formatBluetoothAddr(const uint8_t* address) {
  char hexAddress[ADDRESS_LENGTH*3];
  char* part = hexAddress;
  
  part += sprintf(part,"%02X",address[0]);

  for(int i = 1; i < ADDRESS_LENGTH; i++) {
    part += sprintf(part,":%02X",address[i]);
  }
  
  return String(hexAddress);
}*/

void setup() {
  Serial.begin(BAUD_RATE);
  Serial.println("Sensor BLE server starting");
  BLEDevice::init(SERVER_NAME);

  //const uint8_t* addr = esp_bt_dev_get_address();
  //String addrStr = formatBluetoothAddr(addr);

  Serial.print("Address: ");
  Serial.println(BLEDevice::getAddress().toString().c_str());

  //Create characteristic for temperature
  BLEServer *server = BLEDevice::createServer();
  BLEService *service = server->createService(SERVICE_UUID);
  chara = service->createCharacteristic(
    CHARA_UUID, BLECharacteristic::PROPERTY_NOTIFY);

  //Send advertisements of beacon info
  service->start();
  BLEAdvertising *advert = server->getAdvertising();
  BLEAdvertisementData advertisementData;
  advertisementData.setManufacturerData(
    MODEL_NUMBER"|"SERIAL_NUMBER);
  
  advert->setAdvertisementData(advertisementData);
  advert->addServiceUUID(SERVICE_UUID);
  advert->start();

  Serial.println("Started Advertising");
}

void loop() {
  //For each second, update characteristic
  delay(1000);

  //Get temperature
  int analogTemp = analogRead(TEMP_PIN);
  float percentTemp = (analogTemp/TEMP_MAX_ANALOG_VALUE);
  float temp = percentTemp*TEMP_RANGE+TEMP_MIN;
  Serial.print("Changing to: ");
  Serial.println(temp);

  //Update characteristic value with the temperature value
  chara->setValue(temp);
  chara->notify();
}

