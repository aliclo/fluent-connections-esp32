#pragma once

template <typename T>
class AbstractElem {
  private:
  AbstractElem<T>* next = NULL;
  AbstractElem<T>* prev = NULL;

  public:
  AbstractElem();

  ~AbstractElem();

  virtual AbstractElem<T>* getNext();

  virtual AbstractElem<T>* getPrevious();

  void setNext(AbstractElem<T>* genericElem);

  void setPrevious(AbstractElem<T>* genericElem);
};

template <typename T>
class GenericElem : public AbstractElem<T> {
  protected:
  T* v;

  public:
  GenericElem(T* v);

  ~GenericElem();

  GenericElem<T>* getNext() override;

  GenericElem<T>* getPrevious() override;

  /*void setNext(GenericElem<T>* genericElem) override;

  void setPrevious(GenericElem<T>* genericElem) override;*/

  T* getValue();

  void setValue(T* v);
};

template <typename T>
class ValueElem : public AbstractElem<T> {
  protected:
  T v;

  public:
  ValueElem(T v);

  ~ValueElem();

  ValueElem<T>* getNext() override;

  ValueElem<T>* getPrevious() override;

  /*void setNext(GenericElem<T>* genericElem) override;

  void setPrevious(GenericElem<T>* genericElem) override;*/

  T getValue();

  void setValue(T v);
};

template <typename T>
class GenericDestroyableElem : public GenericElem<T> {
  public:
  GenericDestroyableElem(T* v);
  
  ~GenericDestroyableElem();
};

/*template <typename T>
class GenericStaticSingleElem : public GenericElem<T> {
  public:
  GenericStaticSingleElem(T* v);
  
  ~GenericStaticSingleElem();
};*/

template <typename T>
class GenericDestroyableMalloc : public GenericElem<T> {
  public:
  GenericDestroyableMalloc(T* v);
  
  ~GenericDestroyableMalloc();
};

template <typename T>
class AbstractList {
  private:
  AbstractElem<T>* tail = NULL;
  AbstractElem<T>* head = NULL;
  int count = 0;

  void destroyElements();

  protected:
  void placeElement(AbstractElem<T>* elem);
  
  public:
  ~AbstractList();

  void removeElem(AbstractElem<T>* elem);

  void empty();

  AbstractElem<T>* getHead();

  int getCount();

  void removeHead();
};

template <typename T>
class GenericList : public AbstractList<T> {
  public:
  ~GenericList();

  void insertDestroyable(T* v);

  void insert(T* v);

  void insertDestroyableMalloc(T* v);

  GenericElem<T>* getHead();

  T* getHeadValue();
};

template <typename T>
class ValueList : public AbstractList<T> {
  public:
  ~ValueList();

  void insert(T v);

  ValueElem<T>* getHead();

  T getHeadValue();
};

template <typename T>
class ListIterator {
  private:
  GenericElem<T>* current;

  public:
  ListIterator(GenericList<T>* list);

  ~ListIterator();

  T* getNext();
};

class CharList : public GenericList<char> {
  public:
  ~CharList();

  char* toString();
};

class StringElem {
  private:
  StringElem* next = NULL;
  char* str;

  public:
  StringElem(char* str);

  ~StringElem();

  StringElem* getNext();

  void setNext(StringElem* stringElem);

  char* getValue();
};

class StringList {
  private:
  StringElem* tail = NULL;
  StringElem* head = NULL;
  int count = 0;
  int charCount = 0;

  public:
  ~StringList();
  
  void insertString(char* str);

  char** toArray();
  
  char* toString();

  int getCount();
  
  int getCharCount();
};

bool startsFrom(int pos, const char* f, int fn, const char* s, int sn);

StringList* split(char* s, int sn, const char** ds, int* dns, int dn);

template <typename T>
class LenArray {
  private:
  T* contents;
  int len;

  public:
  LenArray(T* contents, int len);
  
  T* getContents();

  int getLength();
};

class SplitStream {
  private:
  CharList chars;
  GenericList<char> splits;
  ValueList<int> lengths;
  char* delimiter;
  int lastDelimiterIndex;
  ValueList<int> checkPositions;
  
  public:
  SplitStream(char* delimiter, int delimiterLength);
  
  void insertChar(char* c);

  void endReached();

  LenArray<char>* next();
};

#include "DataStructures_impl.h"
