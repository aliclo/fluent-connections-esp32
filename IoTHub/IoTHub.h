#pragma once

#include "esp_gattc_api.h"
#include "esp_gap_ble_api.h"
#include <unordered_map>

//constexpr size_t hashString(const char* string);

const int HEX_UUID_LEN = 32;
const int HEX_UUID_STR_LEN = HEX_UUID_LEN+1;
const int BYTE_UUID_LEN = 16;

const int HEX_PER_BYTE = 2;
const int BITS_PER_HEX = 4;

const int HEX_ADDR_LEN = ESP_BD_ADDR_LEN*HEX_PER_BYTE;
const int HEX_ADDR_STR_LEN = HEX_ADDR_LEN+1;

void setCharaFilter(esp_bt_uuid_t* charaFilter, uint8_t* uuid);

uint8_t** getUUIDs(char* uuidsStr, int* numOfUUIDsRef);

void convertToHex(char* str, int len, char* hex);

void convertHexToBytes(char* hex, uint8_t* result, int hexLen);

uint8_t convertHexPairToByte(char* hex);

uint8_t convertHexToInt(int c);

size_t hashString(const char* string);

class StringHasher {
  public:
  size_t operator() (const char* string) const;
};

class StringComparator {
  public:
  bool operator() (const char* x, const char* y) const;
};

class AddressHasher {
  public:
  size_t operator() (const uint8_t* address) const;
};

class AddressComparator {
  public:
  bool operator() (const uint8_t* x, const uint8_t* y) const;
};

class Device;

class Characteristic {
  private:
  uint8_t* uuid;
  //uint16_t handle;
  uint8_t* value;
  uint16_t valueLength;
  Device* device;

  public:
  Characteristic(Device* device, uint8_t* uuid);

  uint8_t* getUUID();

  //setHandle(uint16_t handle);

  void setValue(uint8_t* value, uint16_t valueLength);

  void writeData(WiFiClient* cl);
};

class Device {
  private:
  char* name;
  uint8_t address[ESP_BD_ADDR_LEN];
  char hexAddress[HEX_ADDR_STR_LEN];
  char* modelNumber;
  char* serialNumber;
  Characteristic** characteristics;
  int numOfCharacteristics;
  bool updateDue = false;
  WiFiClient cl;

  public:
  static Device* createDevice(uint8_t* data, esp_bd_addr_t address);

  static Device* createDevice(char* address, char* charaUUIDsStr);
  
  char* getName();

  /*uint16_t* getConnId();

  void setConnId(uint16_t* connId);*/

  char* getHexAddress();

  uint8_t* getAddress();

  char* getModelNumber();

  char* getSerialNumber();

  Characteristic** getCharacteristics();

  int getNumOfCharacteristics();

  void setWiFiClient(WiFiClient* cl);

  void onCharacteristicUpdated(Characteristic* chara);

  void update();
  
  private:
  Device(char* name, char* modelNumber, char* serialNumber, uint8_t* address);

  Device(char* address, uint8_t** charaUUIDs, int numOfCharaUUIDS);
};

class Connection {
  private:
  Device* device;
  uint16_t startHandle;
  uint16_t endHandle;
  uint16_t connectionId;

  public:
  Connection(Device* device, uint16_t connectionId);

  Device* getDevice();

  uint16_t getConnectionId();

  uint16_t getStartHandle();

  void setStartHandle(uint16_t startHandle);

  uint16_t getEndHandle();

  void setEndHandle(uint16_t endHandle);
};

/*class CharaHandle {
  private:
  Connection* connection;
  uint16_t handle;

  public:
  CharaHandle(Connection* connection);

  Connection* getConnection();

  uint16_t getHandle();

  void setHandle(uint16_t handle);
};*/

typedef struct {
  esp_gattc_cb_t callback;
  uint16_t interface;
  uint16_t appId;
  uint16_t serviceStartHandle;
  uint16_t serviceEndHandle;
  uint16_t charHandle;
  std::unordered_map<char*, Device*, StringHasher, StringComparator> devices;
  //std::unordered_map<char*, Device*, StringHasher, StringComparator> modelSerialDevices;
} GattcProfile;

class HttpResponse {
  public:
  static const int SUCCESS = 2;
  static const int REDIRECTION = 3;
  static const int CLIENT_ERROR = 4;
  static const int SERVER_ERROR = 5;

  static String getStatusTypeDesc(int s);
  
  HttpResponse(char* str);

  ~HttpResponse();

  int getStatusCodeType();

  long getStatusCode();

  void setStatusCode(long statusCode);

  long getContentLength();

  protected:
  HttpResponse();
  
  char** parseResponse(char* str);
  
  private:
  long statusCode = -1;
  int statusCodeType = -1;
  long contentLength = -1;
};

class HttpAuthenticationResponse : public HttpResponse {
  public:
  HttpAuthenticationResponse(char* str);

  ~HttpAuthenticationResponse();

  char* getAuthenticationToken();
  
  private:
  char* authenticationToken;
};

void handleRegisterResponse(HttpResponse* response);

void handleCheckSignedInResponse(HttpResponse* response);

void handleSignInResponse(HttpAuthenticationResponse* response);

typedef std::unordered_map<char*, char*, StringHasher, StringComparator> Parameters;

class WiFiConnection;

typedef void (*RequestHandler) (WiFiConnection*);
typedef void (*WiFiCloseHandler) (Parameters*);
typedef void (*ResponseHandler) (HttpResponse*);
typedef void (*AuthenticationResponseHandler) (HttpAuthenticationResponse*);

class WebMethod {
  public:
  WebMethod(RequestHandler requestHandler, WiFiCloseHandler wifiCloseHandler);

  void handleRequest(WiFiConnection* wifiConnection);

  void handleClose(Parameters* parameters);
  
  private:
  RequestHandler requestHandler;
  WiFiCloseHandler wifiCloseHandler;
};

class WiFiConnection {
  public:
  WiFiConnection(WiFiClient* cl, WebMethod* webMethod, Parameters* params);

  ~WiFiConnection();

  uint16_t getBTConnId();

  void setBTConnId(uint16_t btConnId);

  WiFiClient* getWiFiClient();

  Parameters* getParams();

  void handleClose();
  
  private:
  WiFiClient* cl;
  WebMethod* webMethod;
  Parameters* params;
  uint16_t btConnId;
};

/*constexpr size_t hashString(const char* string) {
  size_t hash = 13;

  int i = 0;
  while(string[i] != '\0') {
    hash = hash * 37 + std::hash<char>()(string[i]);
    i++;
  }

  return hash;
}*/


class HttpParameter {
  public:
  HttpParameter(char* paramName, char* paramValue);

  ~HttpParameter();
  
  char* getParamName();

  char* getParamValue();
  
  private:
  char* paramName;
  char* paramValue;
};

class HttpRequest {
  public:
  static const int GET = 0;
  static const int POST = 1;
  static const size_t GET_HASH = 758325;
  static const size_t POST_HASH = 28527639;

  HttpRequest(int type, char* controller, char* action);

  ~HttpRequest();

  static String getRequestTypeStr(int requestType);

  static int getRequestTypeInt(char* requestType);
  
  static HttpRequest* parseRequest(WiFiClient* cl);

  HttpRequest* addQueryParam(const char* paramName, const char* paramValue);

  HttpRequest* addParam(const char* paramName, const char* paramValue);

  HttpRequest* addCookie(const char* cookieName, const char* cookieValue);

  int getType();

  char* getController();

  char* getAction();

  GenericList<HttpParameter>* getQueryParams();

  GenericList<HttpParameter>* getParams();

  GenericList<HttpParameter>* getCookies();

  private:
  int type;
  char* controller;
  char* action;
  GenericList<HttpParameter> queryParams;
  GenericList<HttpParameter> parameters;
  GenericList<HttpParameter> cookies;
};

class SendableHttpRequest : public HttpRequest {
  public:
  SendableHttpRequest(int type, char* controller, char* action, WiFiClient* cl);

  ~SendableHttpRequest();

  SendableHttpRequest* addParam(const char* paramName, const char* paramValue);

  SendableHttpRequest* addCookie(const char* cookieName, const char* cookieValue);

  bool hasSent();

  void generateStringList(StringList* stringList, GenericList<HttpParameter>* params, char* separator);
  
  void sendRequest();

  virtual void handleResponse(char* responseStr) = 0;
  
  private:
  WiFiClient* cl;
  bool sent = false;
};

class HttpStandardRequest : public SendableHttpRequest {
  public:
  HttpStandardRequest(int type, char* controller, char* action, ResponseHandler handler, WiFiClient* cl);

  ~HttpStandardRequest();
  
  void handleResponse(char* responseStr) override;

  protected:
  ResponseHandler handler;
};

class HttpAuthenticationRequest : public SendableHttpRequest {
  public:
  HttpAuthenticationRequest(int type, char* controller, char* action, AuthenticationResponseHandler handler, WiFiClient* cl);

  ~HttpAuthenticationRequest();
  
  void handleResponse(char* responseStr) override;

  protected:
  AuthenticationResponseHandler handler;
};

//As described from https://www.arduino.cc/en/Reference/WiFiStatus
String getWiFiStatusDesc(int s);

static void gattcCallback(esp_gattc_cb_event_t event,
    esp_gatt_if_t interface, esp_ble_gattc_cb_param_t *param);

static void profileHandler(esp_gattc_cb_event_t event,
    esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t* param);

static void gapCallback(esp_gap_ble_cb_event_t event,
    esp_ble_gap_cb_param_t* param);

void initBluetooth();

void handleWiFiConnections();
