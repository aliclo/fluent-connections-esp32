#pragma once

#include <cstddef>
#include <stdlib.h>
#include <string.h>

//Class AbstractElem
template <typename T>
AbstractElem<T>::AbstractElem() {
  
}

template <typename T>
AbstractElem<T>::~AbstractElem() {
  
}

template <typename T>
AbstractElem<T>* AbstractElem<T>::getNext() {
  return next;
}

template <typename T>
AbstractElem<T>* AbstractElem<T>::getPrevious() {
  return prev;
}

template <typename T>
void AbstractElem<T>::setNext(AbstractElem<T>* genericElem) {
  next = genericElem;
}

template <typename T>
void AbstractElem<T>::setPrevious(AbstractElem<T>* genericElem) {
  prev = genericElem;
}

//Class GenericElem
template <typename T>
GenericElem<T>::GenericElem(T* v) : AbstractElem<T>() {
  this->v = v;
}

template <typename T>
GenericElem<T>::~GenericElem() {
  
}

template <typename T>
GenericElem<T>* GenericElem<T>::getNext() {
  return (GenericElem<T>*) AbstractElem<T>::getNext();
}

template <typename T>
GenericElem<T>* GenericElem<T>::getPrevious() {
  return (GenericElem<T>*) AbstractElem<T>::getPrevious();
}

/*template <typename T>
void GenericElem<T>::setNext(GenericElem<T>* genericElem) {
  next = genericElem;
}

template <typename T>
void GenericElem<T>::setPrevious(GenericElem<T>* genericElem) {
  prev = genericElem;
}*/

template <typename T>
T* GenericElem<T>::getValue() {
  return v;
}

template <typename T>
void GenericElem<T>::setValue(T* v) {
  delete this->v;
  this->v = v;
}

//Class ValueElem
template <typename T>
ValueElem<T>::ValueElem(T v) : AbstractElem<T>() {
  this->v = v;
}

template <typename T>
ValueElem<T>::~ValueElem() {
  
}

template <typename T>
ValueElem<T>* ValueElem<T>::getNext() {
  return (ValueElem<T>*) AbstractElem<T>::getNext();
}

template <typename T>
ValueElem<T>* ValueElem<T>::getPrevious() {
  return (ValueElem<T>*) AbstractElem<T>::getPrevious();
}

template <typename T>
T ValueElem<T>::getValue() {
  return v;
}

template <typename T>
void ValueElem<T>::setValue(T v) {
  this->v = v;
}

//class GenericDestroyableElem
template <typename T>
GenericDestroyableElem<T>::GenericDestroyableElem(T* v) : GenericElem<T>(v) {
  
}

template <typename T>
GenericDestroyableElem<T>::~GenericDestroyableElem() {
  delete this->v;
}

//class GenericStaticSingleElem
/*template <typename T>
GenericStaticSingleElem<T>::GenericStaticSingleElem(T* v) : GenericElem<T>(v) {
  
}

template <typename T>
GenericStaticSingleElem<T>::~GenericStaticSingleElem() {
  
}*/

//class GenericDestroyableMalloc
template <typename T>
GenericDestroyableMalloc<T>::GenericDestroyableMalloc(T* v) : GenericElem<T>(v) {
  
}

template <typename T>
GenericDestroyableMalloc<T>::~GenericDestroyableMalloc() {
  free(this->v);
}

//class AbstractList
template <typename T>
AbstractList<T>::~AbstractList() {
  destroyElements();
}

template <typename T>
void AbstractList<T>::removeElem(AbstractElem<T>* elem) {
  AbstractElem<T>* next = elem->getNext();
  AbstractElem<T>* previous = elem->getPrevious();

  if(next == NULL) {
    tail = previous;
  } else {
    next->setPrevious(previous);
  }

  if(previous == NULL) {
    head = next;
  } else {
    previous->setNext(next);
  }

  count--;
}

template <typename T>
void AbstractList<T>::empty() {
  destroyElements();
  
  head = NULL;
  tail = NULL;
  count = 0;
}

template <typename T>
AbstractElem<T>* AbstractList<T>::getHead() {
  return head;
}

template <typename T>
int AbstractList<T>::getCount() {
  return count;
}

template <typename T>
void AbstractList<T>::removeHead() {
  AbstractElem<T>* elem = head;
  head = head->getNext();
  
  //If there is no head, there should be no tail
  if(head == NULL) {
    tail = NULL;
  }
  
  delete elem;
  count--;
}

template <typename T>
void AbstractList<T>::destroyElements() {
  AbstractElem<T>* current = head;

  while(current != NULL) {
    AbstractElem<T>* temp = current;
    current = current->getNext();
    delete temp;
  }
}

template <typename T>
void AbstractList<T>::placeElement(AbstractElem<T>* elem) {
  if(tail == NULL) {
    head = elem;
  } else {
    tail->setNext(elem);
    elem->setPrevious(tail);
  }

  tail = elem;
  count++;
}

//class GenericList
template <typename T>
GenericList<T>::~GenericList() {
  
}

template <typename T>
void GenericList<T>::insertDestroyable(T* v) {
  GenericDestroyableElem<T>* genericElem = new GenericDestroyableElem<T>(v);
  AbstractList<T>::placeElement(genericElem);
}

template <typename T>
void GenericList<T>::insert(T* v) {
  GenericElem<T>* genericElem = new GenericElem<T>(v);
  AbstractList<T>::placeElement(genericElem);
}

template <typename T>
void GenericList<T>::insertDestroyableMalloc(T* v) {
  GenericDestroyableMalloc<T>* genericElem = new GenericDestroyableMalloc<T>(v);
  AbstractList<T>::placeElement(genericElem);
}

template <typename T>
GenericElem<T>* GenericList<T>::getHead() {
  return (GenericElem<T>*) AbstractList<T>::getHead();
}

template <typename T>
T* GenericList<T>::getHeadValue() {
  if(getHead() == NULL) {
    return NULL;
  }
  
  return getHead()->getValue();
}

//class ValueList
template <typename T>
ValueList<T>::~ValueList() {
  
}

template <typename T>
void ValueList<T>::insert(T v) {
  ValueElem<T>* valueElem = new ValueElem<T>(v);
  AbstractList<T>::placeElement(valueElem);
}

template <typename T>
ValueElem<T>* ValueList<T>::getHead() {
  return (ValueElem<T>*) AbstractList<T>::getHead();
}

template <typename T>
T ValueList<T>::getHeadValue() {
  if(getHead() == NULL) {
    return NULL;
  }
  
  return getHead()->getValue();
}

//class ListIterator
template <typename T>
ListIterator<T>::ListIterator(GenericList<T>* list) {
  this->current = list->getHead();
}

template <typename T>
ListIterator<T>::~ListIterator() {
  
}

template <typename T>
T* ListIterator<T>::getNext() {
  if(current == NULL) {
    return NULL;
  } else {
    T* value = current->getValue();
    current = current->getNext();
    return value;
  }
}

bool startsFrom(int pos, const char* f, int fn, const char* s, int sn) {
  int endLen = pos+fn;

  if(endLen > sn) {
    return false;
  }

  int fi = 0;
  for(int si = pos; si < endLen; si++) {
    if(*(f+fi) != *(s+si)) {
      return false;
    }

    fi++;
  }

  return true;
}

//class CharList
CharList::~CharList() {
  
}

char* CharList::toString() {
  char* string = (char*) malloc(getCount()+1);

  GenericElem<char>* elem = getHead();

  for(int i = 0; i < getCount(); i++) {
    *(string+i) = *(elem->getValue());
    elem = elem->getNext();
  }

  *(string+getCount()) = '\0';

  return string;
}

//class StringElem
StringElem::~StringElem() {
  //free(str);
}

StringElem::StringElem(char* str) {
  this->str = str;
}

StringElem* StringElem::getNext() {
  return next;
}

void StringElem::setNext(StringElem* stringElem) {
  next = stringElem;
}

char* StringElem::getValue() {
  return str;
}

//class StringList
StringList::~StringList() {
  StringElem* current = head;

  while(current != NULL) {
    StringElem* temp = current;
    current = current->getNext();
    delete temp;
  }
}

void StringList::insertString(char* str) {
  StringElem* stringElem = new StringElem(str);
  if(tail == NULL) {
    head = stringElem;
  } else {
    tail->setNext(stringElem);
  }

  tail = stringElem;
  count++;

  charCount += strlen(str);
}

char** StringList::toArray() {
  char** arr = (char**) malloc(count*sizeof(char*));

  StringElem* elem = head;

  for(int i = 0; i < count; i++) {
    *(arr+i) = elem->getValue();
    elem = elem->getNext();
  }

  return arr;
}

char* StringList::toString() {
  if(charCount == 0) {
    return "";
  }
  
  char* str = (char*) malloc((charCount+1)*sizeof(char));

  StringElem* elem = head;
  
  strcpy(str, elem->getValue());
  
  for(int i = 1; i < count; i++) {
    elem = elem->getNext();
    strcat(str, elem->getValue());
  }

  return str;
}

int StringList::getCount() {
  return count;
}

int StringList::getCharCount() {
  return charCount;
}

StringList* split(char* s, int sn, const char** ds, int* dns, int dn) {
  StringList* list = new StringList();
  CharList charList = CharList();

  int si = 0;
  while(si < sn) {
    int di = 0;
    bool found = false;

    while(di < dn && !found) {
      int delLen = *(dns+di);
      if(startsFrom(si, *(ds+di), delLen, s, sn)) {
        found = true;
        si += delLen;
      } else {
        di++;
      }
    }

    if(found) {
      list->insertString(charList.toString());
      charList.empty();
    } else {
      charList.insert(s+si);
      si++;
    }
  }

  if(charList.getCount() > 0) {
    list->insertString(charList.toString());
  }

  return list;
}

//class LenArray
template <typename T>
LenArray<T>::LenArray(T* contents, int len) {
  this->contents = contents;
  this->len = len;
}

template <typename T>
T* LenArray<T>::getContents() {
  return contents;
}

template <typename T>
int LenArray<T>::getLength() {
  return len;
}

//class SplitStream
SplitStream::SplitStream(char* delimiter, int delimiterLength) {
  this->delimiter = delimiter;
  this->lastDelimiterIndex = delimiterLength-1;
  checkPositions.insert(0);
}

void SplitStream::insertChar(char* c) {
  chars.insertDestroyable(c);

  ValueElem<int>* elem = checkPositions.getHead();

  bool findingMatch = true;

  while(findingMatch && elem != NULL) {
    ValueElem<int>* next = elem->getNext();
    int pos = elem->getValue();
    if(delimiter[pos] == *c) {
      if(pos == lastDelimiterIndex) {
        findingMatch = false;
      } else {
        elem->setValue(pos+1);
      }
    } else {
      checkPositions.removeElem(elem);
    }
    elem = next;
  }
  
  if(!findingMatch) {
    checkPositions.empty();
    char* split = chars.toString();
    int len = chars.getCount()-this->lastDelimiterIndex-1;
    split[len] = '\0';
    splits.insert(split);
    lengths.insert(len);
    chars.empty();
  }

  checkPositions.insert(0);
}

void SplitStream::endReached() {
  checkPositions.empty();
  splits.insert(chars.toString());
  lengths.insert(chars.getCount());
  chars.empty();
}

LenArray<char>* SplitStream::next() {
  char* split = splits.getHeadValue();
  int len = lengths.getHeadValue();
  
  if(split == NULL) {
    return NULL;
  }

  splits.removeHead();
  lengths.removeHead();

  LenArray<char>* arr = new LenArray<char>(split, len);
  
  return arr;
}
