#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>
#include "DataStructures.h"
#include "IoTHub.h"

#include "nvs.h"
#include "nvs_flash.h"
#include "bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"

#define BAUD_RATE 115200
#define CLIENT_NAME "BLE Client HFAR52"

#define SCAN_DURATION 30

#define TO_STRING(n) #n
#define S(n) TO_STRING(n)

//HARDCODED DETAILS FOR WIFI CONNECTION
#define WIFI_SSID "testnet"
#define WIFI_PASSWORD "LURI@19528"

//Address and port to the web server
#define SERVER "192.168.137.1"
#define PORT 61138
#define HOST SERVER ":" S(PORT)
#define HOST_FIELD "Host: " HOST

//Amount of time between checks for WIFI connection
#define CHECK_WIFI_STATUS_DELAY_MILLISEC 1000

//Amount of time to give for connection
#define CONNECT_TIMEOUT_MILLISEC 20000
#define CONNECT_TIMEOUT_COUNT (CONNECT_TIMEOUT_MILLISEC/ \
  CHECK_WIFI_STATUS_DELAY_MILLISEC)

//#define APP_ID 435452
#define APP_ID 0
#define NUM_OF_PROFILES 1

//Used for converting hex values (represented in ASCII) to int values

//ASCII letters '0' to '9' start from code 48 and end at code 57
#define HEX_NUM_ASCII_START 48

//ASCII letters 'A' to 'Z' start from code 65 and end at code 90
#define HEX_LET_ASCII_START 65

//Numerical values representing hex start from 0 to 9
#define HEX_NUM_VALUE_START 0

//Alphabetical values representing hex start from 10 to 15
#define HEX_LET_VALUE_START 10

#define HEX_BASE 16
#define HEX_BIT_MASK 15

//Clock for relaying updated characteristics
#define CLOCK_ID 0

//Divide frequency using a prescaler to get a resulting frequency of 1 MHz
#define CLOCK_PRESCALER 80

//Timer will count up
#define CLOCK_COUNT_UP true

//Trigger on the edge of the clock signal
#define CLOCK_EDGE_TRIGGER true

//Delay between triggers
#define CLOCK_TRIGGER_MICROSECONDS 1000000

//Repeat clock signals
#define CLOCK_LOOP true

//Find service with UUID
#define SERVICE_UUID {0x4b, 0x91, 0x31, 0xc3, 0xc9, 0xc5, 0xcc, 0x8f, 0x9e, \
  0x45, 0xb5, 0x1f, 0x01, 0xc2, 0xaf, 0x4f}

#define BLANK_UUID {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

//Mapping from base 10 to hex (in ASCII)
const char HEX_MAP[] = {'0','1','2','3','4','5','6', '7', '8', '9', 'A', \
  'B', 'C', 'D', 'E', 'F'};

//List of devices with updated characteristics
GenericList<Device> updateDevices;

//Flag to indicate when to relay characteristics of the devices
bool updateDevicesTime = false;
hw_timer_t* updateTimer;

//Triggered from timer, flags for the relay of characteristics
void IRAM_ATTR onUpdate() {
  updateDevicesTime = true;
}

//Filter used to search for services with the given UUID
static esp_bt_uuid_t serviceFilter = {
  .len = ESP_UUID_LEN_128,
  .uuid = {.uuid128 = SERVICE_UUID}
};

//Client representing hub for sending requests to the web server
WiFiClient wifiClient;

//Server representing hub for receiving requests
WiFiServer wifiServer(80);

//Client requesting for scan for BLE devices
WiFiClient* scanClient;

//List of requests to send
GenericList<SendableHttpRequest> httpRequestsQueue;

//Authentication token given from web server
char* deviceAuthenticationToken;

//Mapping from web requests to methods
std::unordered_map<char*, WebMethod*, StringHasher, StringComparator> webMethodsMap;

//Methods that can be called from web requests
WebMethod scanBeaconsWebMethod(scanBeacons, closeScanBeacons);
WebMethod sensorDataWebMethod(sensorData, closeSensorData);

//Devices that were discovered during the scan, indexed using the Bluetooth
//address of the device
std::unordered_map<uint8_t*, Device*, AddressHasher, AddressComparator>* scannedDevices;

//Connections with the hub, indexed using connection ids
std::unordered_map<uint16_t, Connection*> connections;

//Characteristics of each of the discovered devices, indexed with their handles
std::unordered_map<uint16_t, Characteristic*> charaHandles;

//Links the WIFI and Bluetooth connections together for relaying data
std::unordered_map<uint8_t*, WiFiConnection*, AddressHasher, AddressComparator> bluetoothWiFiConnections;

//List of connections from given requests
GenericList<WiFiConnection> wifiConnections;

//Sets the characteristic filter using the given uuid
void setCharaFilter(esp_bt_uuid_t* charaFilter, uint8_t* uuid) {
  for(int i = 0; i < BYTE_UUID_LEN; i++) {
    charaFilter->uuid.uuid128[i] = uuid[i];
  }
}

//Converts the given string of addresses (that are in hex) to an array of
//addresses in bytes
uint8_t** getUUIDs(char* uuidsStr, int* numOfUUIDsRef) {
  int numOfUUIDs = strlen(uuidsStr)/HEX_UUID_LEN;

  //Array of uuids (each element is a set of bytes representing a uuid)
  uint8_t** uuids = (uint8_t**) malloc(sizeof(uint8_t*)*numOfUUIDs);

  for(int i = 0; i < numOfUUIDs; i++) {
    //Allocate space for uuid
    uint8_t* uuid = (uint8_t*) malloc(sizeof(uint8_t)*BYTE_UUID_LEN);

    //Start index of hex uuid in string
    int startIndex = i*HEX_UUID_LEN;
    char* uuidsStrStart = uuidsStr+startIndex;
    convertHexToBytes(uuidsStrStart, uuid, HEX_UUID_LEN);
    uuids[i] = uuid;
  }

  *numOfUUIDsRef = numOfUUIDs;

  return uuids;
}

//Converts a given string of bytes to a string of hex with the
//given length (including null character)
void convertToHex(char* str, int len, char* hex) {
  //Length excluding null character
  int hexLen = len-1;
  int charIndex = 0;

  //Get the pairs of hex values for each byte from the string
  for(int i = 0; i < hexLen; i += HEX_PER_BYTE) {
    //Get byte
    char c = str[charIndex];

    //Get first and second half of byte
    int firstIndex = i;
    int secondIndex = i+1;
    
    int first = c >> BITS_PER_HEX;
    int second = c & HEX_BIT_MASK;

    //Map both of the two halfs to hex
    hex[firstIndex] = HEX_MAP[first];
    hex[secondIndex] = HEX_MAP[second];

    //Look for next byte
    charIndex++;
  }

  hex[hexLen] = '\0';
}

//Converts a given string of hex to a string of bytes with the
//given length
void convertHexToBytes(char* hex, uint8_t* result, int hexLen) {
  int addrIndex = 0;

  //Convert the pairs of hex values to bytes
  for(int i = 0; i < hexLen; i += HEX_PER_BYTE) {
    result[addrIndex] = convertHexPairToByte(hex + i);

    //Next byte
    addrIndex++;
  }
}

//Converts a pair of hex values into a byte
uint8_t convertHexPairToByte(char* hex) {
  //Get first and second hex values
  int firstIndex = 0;
  int secondIndex = 1;
  char firstHex = hex[firstIndex];
  char secondHex = hex[secondIndex];

  //Combine to construct byte
  return (convertHexToInt(firstHex) << BITS_PER_HEX)
    + convertHexToInt(secondHex);
}

//Converts a given hex value (in ASCII) to an int
uint8_t convertHexToInt(int c) {
  //If the hex representation uses the alphabet, it's in the range 10 to 15
  if(c >= HEX_LET_ASCII_START) {
    return c-HEX_LET_ASCII_START+HEX_LET_VALUE_START;
  //Otherwise, it's in the range 0 to 9
  } else {
    return c-HEX_NUM_ASCII_START+HEX_NUM_VALUE_START;
  }
}

//Hashes the given string
size_t hashString(const char* string) {
  size_t hash = 13;

  int i = 0;
  while(string[i] != '\0') {
    hash = hash * 37 + std::hash<char>()(string[i]);
    i++;
  }

  return hash;
}

//class StringHasher
size_t StringHasher::operator() (const char* string) const {
  return hashString(string);
}

//class StringComparator
bool StringComparator::operator() (const char* x, const char* y) const {
  if(x == y) {
    return true;
  }

  //Checks if all characters of both strings match at the same indices
  int i = 0;
  while(x[i] != '\0' && y[i] != '\0') {
    if(x[i] != y[i]) {
      return false;
    }

    i++;
  }

  return true;
}

//class AddressHasher

//Hashes a given address
size_t AddressHasher::operator() (const uint8_t* address) const {
  size_t hash = 13;

  for(int i = 0; i < ESP_BD_ADDR_LEN; i++) {
    hash = hash * 37 + std::hash<uint8_t>()(address[i]);
  }

  return hash;
}

//class AddressComparator
bool AddressComparator::operator() (const uint8_t* x, const uint8_t* y) const {
  if(x == y) {
    return true;
  }

  //Checks if all bytes of both addresses match at the same indices
  for(int i = 0; i < ESP_BD_ADDR_LEN; i++) {
    if(x[i] != y[i]) {
      return false;
    }
  }

  return true;
}

//class Connection
Connection::Connection(Device* device, uint16_t connectionId) {
  this->device = device;
  this->connectionId = connectionId;
}

Device* Connection::getDevice() {
  return device;
}

uint16_t Connection::getConnectionId() {
  return connectionId;
}

uint16_t Connection::getStartHandle() {
  return startHandle;
}

void Connection::setStartHandle(uint16_t startHandle) {
  this->startHandle = startHandle;
}

uint16_t Connection::getEndHandle() {
  return endHandle;
}

void Connection::setEndHandle(uint16_t endHandle) {
  this->endHandle = endHandle;
}

//class WebMethod
//Handles given requests and handles the cases where the given
//connection becomes closed

WebMethod::WebMethod(RequestHandler requestHandler,
    WiFiCloseHandler wifiCloseHandler) {
  
  this->requestHandler = requestHandler;
  this->wifiCloseHandler = wifiCloseHandler;
}

void WebMethod::handleRequest(WiFiConnection* conn) {
  requestHandler(conn);
}

void WebMethod::handleClose(Parameters* parameters) {
  wifiCloseHandler(parameters);
}

//class Characteristic
Characteristic::Characteristic(Device* device, uint8_t* uuid) {
  this->device = device;
  this->uuid = uuid;
}

uint8_t* Characteristic::getUUID() {
  return uuid;
}

//Set new value for characteristic
void Characteristic::setValue(uint8_t* value, uint16_t valueLength) {
  //Copy the value and its length for the characteristic
  this->value = (uint8_t*) malloc(sizeof(uint8_t)*valueLength);
  memcpy(this->value, value, valueLength);
  this->valueLength = valueLength;
  Serial.print("Length: ");
  Serial.println(valueLength);

  //Notify of updated characteristic
  device->onCharacteristicUpdated(this);
}

//Send characteristic data to given client connection
void Characteristic::writeData(WiFiClient* cl) {
  Serial.print(value[0]);
  cl->print(value[0]);

  for(int i = 1; i < valueLength; i++) {
    //Includes separators to keep array elements separate
    Serial.print("|");
    Serial.print(value[i]);
    cl->print("|");
    cl->print(value[i]);
  }
  Serial.println();
  cl->println();
}

//class Device
Device* Device::createDevice(uint8_t* deviceData, uint8_t* address) {
  //Get device name
  uint8_t deviceNameLength;
  char* deviceNamePos = (char*) esp_ble_resolve_adv_data(deviceData,
    ESP_BLE_AD_TYPE_NAME_CMPL, &deviceNameLength);

  //The device is required to have a name
  if(deviceNamePos == NULL) {
    return NULL;
  }

  //Get device manufacture information
  uint8_t deviceManufactureLength;
  char* deviceManufacturePos = (char*) esp_ble_resolve_adv_data(deviceData,
    ESP_BLE_AD_MANUFACTURER_SPECIFIC_TYPE,
    &deviceManufactureLength);

  //The device is required to provide manufacture information
  if(deviceManufacturePos == NULL) {
    return NULL;
  }

  //Copy device details
  char* deviceName;
  char* modelNumber;
  char* serialNumber;
  
  deviceName = (char*) malloc(sizeof(char)*deviceNameLength);
  memcpy(deviceName, deviceNamePos, deviceNameLength+1);
  *(deviceName+deviceNameLength) = '\0';

  //Obtain model number and serial number from manufacture information
  //(seperated by '|')
  char* sepPos = strchr(deviceManufacturePos, '|');

  //The device is required to provide both model number and serial number
  if(sepPos == NULL ||
      sepPos >= deviceManufacturePos+deviceManufactureLength-1) {

    return NULL;
  }

  //Copy model number
  int modelNumberLength = (int) (sepPos-deviceManufacturePos);
  modelNumber = (char*) malloc(sizeof(char)*modelNumberLength+1);

  memcpy(modelNumber, deviceManufacturePos, modelNumberLength);
  *(modelNumber+modelNumberLength) = '\0';

  //Copy serial number
  int serialNumberLength = deviceManufactureLength-modelNumberLength-1;
  serialNumber = (char*) malloc(sizeof(char)*serialNumberLength+1);

  memcpy(serialNumber, sepPos+1, serialNumberLength);
  *(serialNumber+serialNumberLength) = '\0';
  
  return new Device(deviceName, modelNumber, serialNumber, address);
}

Device* Device::createDevice(char* address, char* charaUUIDsStr) {
  int numOfCharaUUIDs;
  uint8_t** charaUUIDs = getUUIDs(charaUUIDsStr, &numOfCharaUUIDs);
  Device* device = new Device(address, charaUUIDs, numOfCharaUUIDs);
  free(charaUUIDs);
  return device;
}

char* Device::getName() {
  return name;
}

char* Device::getHexAddress() {
  return hexAddress;
}

uint8_t* Device::getAddress() {
  return address;
}

char* Device::getModelNumber() {
  return modelNumber;
}

char* Device::getSerialNumber() {
  return serialNumber;
}

Characteristic** Device::getCharacteristics() {
  return characteristics;
}

int Device::getNumOfCharacteristics() {
  return numOfCharacteristics;
}

void Device::setWiFiClient(WiFiClient* cl) {
  this->cl = *cl;
}

void Device::onCharacteristicUpdated(Characteristic* chara) {
  //If not already done so, insert this device into the list
  //of devices that need to be updated
  if(!updateDue) {
    updateDue = true;
    updateDevices.insert(this);
  }
}

void Device::update() {
  //Relay all characteristic values
  for(int i = 0; i < numOfCharacteristics; i++) {
    Characteristic* chara = characteristics[i];
    chara->writeData(&cl);
  }
  updateDue = false;
}

Device::Device(char* name, char* modelNumber, char* serialNumber,
    uint8_t* address) {
      
  this->name = name;
  this->modelNumber = (char*) modelNumber;
  this->serialNumber = (char*) serialNumber;

  for(int i = 0; i < ESP_BD_ADDR_LEN; i++) {
    this->address[i] = address[i];
  }
  
  convertToHex((char*) address, HEX_ADDR_STR_LEN, hexAddress);
}

Device::Device(char* hexAddress, uint8_t** charaUUIDs, int numOfCharaUUIDs) {
  for(int i = 0; i < HEX_ADDR_STR_LEN; i++) {
    this->hexAddress[i] = hexAddress[i];
  }

  convertHexToBytes(hexAddress, address, HEX_ADDR_LEN);

  characteristics = (Characteristic**) malloc(sizeof(Characteristic*)
    * numOfCharaUUIDs);
    
  numOfCharacteristics = numOfCharaUUIDs;

  for(int i = 0; i < numOfCharaUUIDs; i++) {
    characteristics[i] = new Characteristic(this, charaUUIDs[i]);
  }
}

//When scanning for devices, perform an active scan with own address
//type as public, allow any advertisements while scanning, and scan
//at intervals of 50 milliseconds (0x50*0.625 = 50 base 10) with a
//window of 30 milliseconds (0x30*0.625 = 30 base 10), and filter
//out duplicate advertisements
static esp_ble_scan_params_t scanParams = {
  .scan_type = BLE_SCAN_TYPE_ACTIVE,
  .own_addr_type = BLE_ADDR_TYPE_PUBLIC,
  .scan_filter_policy = BLE_SCAN_FILTER_ALLOW_ALL,
  .scan_interval = 0x50,
  .scan_window = 0x30,
  .scan_duplicate = BLE_SCAN_DUPLICATE_ENABLE
};

static GattcProfile profiles[NUM_OF_PROFILES] = {
  [APP_ID] = {
    .callback = profileHandler,
    .interface = ESP_GATT_IF_NONE
  }
};

//class HttpResponse
String HttpResponse::getStatusTypeDesc(int s) {
  switch(s) {
    case HttpResponse::SUCCESS:
      return "Success";
    case HttpResponse::REDIRECTION:
      return "Redirection";
    case HttpResponse::CLIENT_ERROR:
      return "Client Error";
    case HttpResponse::SERVER_ERROR:
      return "Server Error";
    default:
      return "Invalid Status Code Type";
  }
}

HttpResponse::HttpResponse(char* str) {
  char** splitStrings = parseResponse(str);
  free(splitStrings);
}

HttpResponse::HttpResponse() {
  
}

HttpResponse::~HttpResponse() {
  
}

char** HttpResponse::parseResponse(char* str) {
  //Split response up to get HTTP status and key value pairs
  const char* delimiterStr = ": ";
  const char* delimiterNewline = "\n";
  const char* delimiter[] = {delimiterStr, delimiterNewline};
  int s[] = {strlen(delimiterStr), strlen(delimiterNewline)};
  int numOfDelimiters = 2;
  
  StringList* splitResult = split(str, strlen(str), delimiter,
    s, numOfDelimiters);
  
  char** splitStrings = splitResult->toArray();

  //Split the HTTP status line
  const char* responseDelimiterStr = " ";
  const char* responseDelimiter[] = {responseDelimiterStr};
  int responseDelimiterLengths[] = {strlen(responseDelimiterStr)};
  int numOfResponseDelimiters = 1;
  char* response = *(splitStrings+0);

  StringList* responseSplitResult = split(response, strlen(response),
    responseDelimiter, responseDelimiterLengths, numOfResponseDelimiters);
  
  char** responseSplitStrings = responseSplitResult->toArray();

  //Get status code
  char* statusCodeStr = *(responseSplitStrings+1);
  
  setStatusCode(strtol(statusCodeStr, NULL, 10));

  free(responseSplitStrings);
  delete responseSplitResult;

  //Find and store content length
  for(int i = 1; i < splitResult->getCount(); i += 2) {
    const char* attributeName = *(splitStrings+i);

    if(strcmp(attributeName, "Content-Length") == 0) {
      const char* value = *(splitStrings+i+1);

      contentLength = strtol(value, NULL, 10);
    }
  }

  delete splitResult;
  
  return splitStrings;
}

//Indicates if status code means: information, success, redirection,
//client error or server error
int HttpResponse::getStatusCodeType() {
  return statusCodeType;
}

long HttpResponse::getStatusCode() {
  return statusCode;
}

void HttpResponse::setStatusCode(long statusCode) {
  this->statusCode = statusCode;
  this->statusCodeType = statusCode/100;
}

long HttpResponse::getContentLength() {
  return contentLength;
}


//class HttpAuthentication
HttpAuthenticationResponse::HttpAuthenticationResponse(char* str)
    : HttpResponse() {
      
  //Extract basic details from response
  char** splitStrings = parseResponse(str);

  if(getStatusCodeType() == HttpResponse::SUCCESS) {
    //Obtain authentication token from response
    char* authenticationCookie = *(splitStrings+10);

    //Given as a key value pair separated with '=' and ending with ';'
    const char* authenticationCookieDelimiterSeperatorStr = "; ";
    const char* authenticationCookieDelimiterEqualsStr = "=";
    
    const char* authenticationCookieDelimiter[] = {
      authenticationCookieDelimiterSeperatorStr,
      authenticationCookieDelimiterEqualsStr};
      
    int authenticationCookieDelimiterLengths[] = {
      strlen(authenticationCookieDelimiterSeperatorStr),
      strlen(authenticationCookieDelimiterEqualsStr)};
      
    int numOfAuthenticationCookieDelimiters = 2;
    
    StringList* authenticationCookieSplitResult = split(authenticationCookie,
      strlen(authenticationCookie), authenticationCookieDelimiter,
      authenticationCookieDelimiterLengths,
      numOfAuthenticationCookieDelimiters);
    
    char** authenticationCookieSplitStrings =
      authenticationCookieSplitResult->toArray();

    //Extract authentication token
    authenticationToken = *(authenticationCookieSplitStrings+1);
    
    free(authenticationCookieSplitStrings);
    delete authenticationCookieSplitResult;
  }
  
  free(splitStrings);
}

HttpAuthenticationResponse::~HttpAuthenticationResponse() {
  
}

char* HttpAuthenticationResponse::getAuthenticationToken() {
  return authenticationToken;
}

//Handle the response from the registration request
void handleRegisterResponse(HttpAuthenticationResponse* response) {
  int statusCodeType = response->getStatusCodeType();
  
  switch(statusCodeType) {
    case HttpResponse::SUCCESS: {
      Serial.println("Successfully Registered");
      //Store authentication token
      deviceAuthenticationToken = response->getAuthenticationToken();

      //Provide ip address to server (to allow the web server to send requests
      //to the hub)
      HttpStandardRequest* request = new HttpStandardRequest(HttpRequest::POST,
        "Account", "UpdateIpAddress", &handleUpdateIpAddress, &wifiClient);

      //Provide ip address and authentication token
      request->addParam("ipaddress", WiFi.localIP().toString().c_str());
      request->addCookie(".AspNetCore.Identity.Application",
        deviceAuthenticationToken);

      //Add request to queue
      httpRequestsQueue.insertDestroyable(request);
      break;
    }
    case HttpResponse::CLIENT_ERROR: {
      Serial.println("Already Registered");

      //If the hub is already registered with the web server, attempt to sign in
      HttpAuthenticationRequest* request = new HttpAuthenticationRequest(
        HttpRequest::POST, "Account", "AuthenticateDevice",
        &handleSignInResponse, &wifiClient);

      //Send authentication details
      request->addParam("username", "test")
        ->addParam("password", "MyPassword%26123");

      //Add request to queue
      httpRequestsQueue.insertDestroyable(request);
      break;
    }
    default:
      Serial.println(HttpResponse::getStatusTypeDesc(statusCodeType));
  }
}

//Handle the response from the sign in request
void handleSignInResponse(HttpAuthenticationResponse* response) {
  //Store authentication token
  deviceAuthenticationToken = response->getAuthenticationToken();

  //Check if the hub has signed in successfully
  HttpStandardRequest* request = new HttpStandardRequest(HttpRequest::GET,
    "Account", "CheckSignedIn", &handleCheckSignIn, &wifiClient);

  //Provide authentication token
  request->addCookie(".AspNetCore.Identity.Application",
    deviceAuthenticationToken);

  //Add request to queue
  httpRequestsQueue.insertDestroyable(request);
}

//Handle the response from the request to check if signed in
void handleCheckSignIn(HttpResponse* response) {
  int statusCodeType = response->getStatusCodeType();

  switch(statusCodeType) {
    //If status is successful, then the hub has successfully signed in
    case HttpResponse::SUCCESS: {
      Serial.println("Successfully signed In");

      //Provide the hub's ip address to the web server
      HttpStandardRequest* request = new HttpStandardRequest(HttpRequest::POST,
        "Account", "UpdateIpAddress", &handleUpdateIpAddress, &wifiClient);

      //Provide ip address and authentication token
      request->addParam("ipaddress", WiFi.localIP().toString().c_str());
      request->addCookie(".AspNetCore.Identity.Application",
        deviceAuthenticationToken);

      //Add request to queue
      httpRequestsQueue.insertDestroyable(request);
      break;
    }
    case HttpResponse::REDIRECTION: {
      Serial.println("Failed to sign in");
      break;
    }
    default:
      Serial.println(HttpResponse::getStatusTypeDesc(statusCodeType));
  }
}

//Handle the response from the request to update ip address
void handleUpdateIpAddress(HttpResponse* response) {
  
}

//class WiFiConnection

//Takes the client connection, the invoked web method and the
//parameters given from the request
WiFiConnection::WiFiConnection(WiFiClient* cl, WebMethod* webMethod,
    Parameters* params) {
      
  this->cl = cl;
  this->webMethod = webMethod;
  this->params = params;
}

WiFiConnection::~WiFiConnection() {
  delete cl;
  delete params;
}

uint16_t WiFiConnection::getBTConnId() {
  return btConnId;
}

void WiFiConnection::setBTConnId(uint16_t btConnId) {
  this->btConnId = btConnId;
}

WiFiClient* WiFiConnection::getWiFiClient() {
  return cl;
}

Parameters* WiFiConnection::getParams() {
  return params;
}

void WiFiConnection::handleClose() {
  webMethod->handleClose(params);
}

//class HttpParameter
HttpParameter::HttpParameter(char* paramName, char* paramValue) {
  this->paramName = paramName;
  this->paramValue = paramValue;
}

HttpParameter::~HttpParameter() {
  free(paramName);
  free(paramValue);
}

char* HttpParameter::getParamName() {
  return paramName;
}

char* HttpParameter::getParamValue() {
  return paramValue;
}

//class HttpRequest

//Get the type of request (POST or GET) as a string from its given ID
String HttpRequest::getRequestTypeStr(int requestType) {
  switch(requestType) {
    case HttpRequest::POST:
      return "POST";
    default:
      return "GET";
  }
}

//Get the ID of the given type of request (given as a string)
int HttpRequest::getRequestTypeInt(char* requestType) {
  switch (hashString(requestType)) {
    case HttpRequest::POST_HASH: return HttpRequest::POST;
    default: return HttpRequest::GET;
  }
}

//Parses the request given from the client connection
HttpRequest* HttpRequest::parseRequest(WiFiClient* cl) {
  bool blankLine = true;
  bool readRequest = false;
  LenArray<char>* responseSplit = NULL;

  //Read response line by line
  SplitStream requestStream("\n", 1);

  Serial.println("REQUEST");

  //Keep reading response until finished reading a line
  while(responseSplit == NULL && cl->connected()) {
    if(cl->available()) {
      char c = cl->read();
      Serial.print(c);
      requestStream.insertChar(new char(c));

      //Attempt to get next line
      responseSplit = requestStream.next();
    }
  }

  //First line provides request type and the resource that is being
  //requested for access, these are separated by spaces
  
  const char* resourceDelimiterSpace = " ";
  const char* resourceDelimiter[] = {resourceDelimiterSpace};
  int resourceDelimiterLength[] = {strlen(resourceDelimiterSpace)};
  int numOfDelimiters = 1;
  
  StringList* splitResult = split(responseSplit->getContents(),
    responseSplit->getLength(), resourceDelimiter, resourceDelimiterLength,
    numOfDelimiters);
  
  char** splitStrings = splitResult->toArray();

  //Extract request type
  char* requestTypeStr = splitStrings[0];

  int requestType = HttpRequest::getRequestTypeInt(requestTypeStr);

  //Get requested controller and action, and get parameters
  char* controllerActionQuery = splitStrings[1];

  //Requested resource and parameters are separated by '?'
  const char* queryDelimiterStr = "?";
  const char* queryDelimiter[] = {queryDelimiterStr};
  int queryDelimiterLength[] = {strlen(queryDelimiterStr)};
  int numOfQueryDelimiters = 1;

  StringList* querySplitResult = split(controllerActionQuery,
    strlen(controllerActionQuery), queryDelimiter, queryDelimiterLength,
    numOfQueryDelimiters);

  char** querySplitStrings = querySplitResult->toArray();

  //Get controller and action (currently only action supported,
  //assumes only one controller)
  char* controllerAction = querySplitStrings[0];
  
  int controllerActionLength = strlen(controllerAction);
  char* action = (char*) malloc(sizeof(char)*controllerActionLength);

  memcpy(action, controllerAction+1, controllerActionLength);

  HttpRequest* request = new HttpRequest(requestType, NULL, action);

  //Check if parameters have been given
  if(querySplitResult->getCount() > 1) {
    char* queryParams = querySplitStrings[1];

    //Extract each parameter, parameters are separated by '&'
    const char* queryParamsDelimiterStr = "&";
    const char* queryParamsDelimiter[] = {queryParamsDelimiterStr};
    int queryParamsDelimiterLength[] = {strlen(queryParamsDelimiterStr)};
    int numOfQueryParamsDelimiters = 1;
  
    StringList* queryParamsSplitResult = split(queryParams, strlen(queryParams),
      queryParamsDelimiter, queryParamsDelimiterLength,
      numOfQueryParamsDelimiters);
  
    char** queryParamsSplitStrings = queryParamsSplitResult->toArray();

    //Get the key value pairs of the parameters
    for(int i = 0; i < queryParamsSplitResult->getCount(); i++) {
      char* param = queryParamsSplitStrings[i];
      int paramLen = strlen(param);

      //Key and value are separated by '='
      char* equalPos = strchr(param, '=');

      //Calculate length of key and value strings
      int nameLen = equalPos-param;
      int valueLen = paramLen-(nameLen+1);
  
      char* name = (char*) malloc(sizeof(char)*(nameLen+1));
      char* value = (char*) malloc(sizeof(char)*(valueLen+1));

      //Extract key and value
      memcpy(name, param, nameLen);
      *(name+nameLen) = '\0';
      memcpy(value, (char*) (param+paramLen-valueLen), valueLen);
      *(value+valueLen) = '\0';
  
      request->addQueryParam(name, value);
  
      Serial.print("Name: ");
      Serial.println(name);
  
      Serial.print("Value: ");
      Serial.println(value);
    }
  
    for(int i = 0; i < queryParamsSplitResult->getCount(); i++) {
      free(queryParamsSplitStrings[i]);
    }
  
    free(queryParamsSplitStrings);
    delete queryParamsSplitResult;
  }

  for(int i = 0; i < querySplitResult->getCount(); i++) {
    free(querySplitStrings[i]);
  }

  free(querySplitStrings);
  delete querySplitResult;

  for(int i = 0; i < splitResult->getCount(); i++) {
    free(splitStrings[i]);
  }

  free(splitStrings);
  delete splitResult;

  //Read rest of request
  while(!readRequest && cl->connected()) {
    if(cl->available()) {
      char c = cl->read();
      Serial.print(c);
      requestStream.insertChar(new char(c));

      if(c == '\n') {
        if(blankLine) {
          Serial.println("END OF REQUEST");
          requestStream.endReached();
          
          readRequest = true;
        } else {
          blankLine = true;
        }
      } else if (c != '\r') {
        blankLine = false;
      }
    }
  }

  return request;
}

HttpRequest::HttpRequest(int type, char* controller, char* action) {
  this->type = type;
  this->controller = controller;
  this->action = action;
}

HttpRequest* HttpRequest::addQueryParam(const char* paramName,
    const char* paramValue) {
      
  //Stores copies of key value pair
  
  //Length includes terminator character
  int len = strlen(paramName)+1;
  char* paramNameCopy = (char*) malloc(sizeof(char)*len);
  strncpy(paramNameCopy, paramName, len);

  //Length includes terminator character
  len = strlen(paramValue)+1;
  char* paramValueCopy = (char*) malloc(sizeof(char)*len);
  strncpy(paramValueCopy, paramValue, len);
  
  HttpParameter* param = new HttpParameter(paramNameCopy, paramValueCopy);
  queryParams.insertDestroyable(param);
  return this;
}

HttpRequest* HttpRequest::addParam(const char* paramName,
    const char* paramValue) {

  //Stores copies of key value pair
  
  //Length includes terminator character
  int len = strlen(paramName)+1;
  char* paramNameCopy = (char*) malloc(sizeof(char)*len);
  strncpy(paramNameCopy, paramName, len);

  //Length includes terminator character
  len = strlen(paramValue)+1;
  char* paramValueCopy = (char*) malloc(sizeof(char)*len);
  strncpy(paramValueCopy, paramValue, len);
  
  HttpParameter* param = new HttpParameter(paramNameCopy, paramValueCopy);
  parameters.insertDestroyable(param);
  return this;
}

HttpRequest* HttpRequest::addCookie(const char* cookieName, const char* cookieValue) {
  //Length includes terminator character
  int len = strlen(cookieName)+1;
  char* cookieNameCopy = (char*) malloc(sizeof(char)*len);
  strncpy(cookieNameCopy, cookieName, len);

  //Length includes terminator character
  len = strlen(cookieValue)+1;
  char* cookieValueCopy = (char*) malloc(sizeof(char)*len);
  strncpy(cookieValueCopy, cookieValue, len);
  
  HttpParameter* cookie = new HttpParameter(cookieNameCopy, cookieValueCopy);
  cookies.insertDestroyable(cookie);
  return this;
}

int HttpRequest::getType() {
  return type;
}

char* HttpRequest::getController() {
  return controller;
}

char* HttpRequest::getAction() {
  return action;
}

GenericList<HttpParameter>* HttpRequest::getQueryParams() {
  return &queryParams;
}

GenericList<HttpParameter>* HttpRequest::getParams() {
  return &parameters;
}

GenericList<HttpParameter>* HttpRequest::getCookies() {
  return &cookies;
}

//class SendableHttpRequest
SendableHttpRequest::SendableHttpRequest(int type, char* controller, char* action, WiFiClient* cl)
    : HttpRequest(type, controller, action) {
  
  this->cl = cl;
}

SendableHttpRequest::~SendableHttpRequest() {
  
}

SendableHttpRequest* SendableHttpRequest::addParam(const char* paramName, const char* paramValue) {
  return (SendableHttpRequest*) HttpRequest::addParam(paramName, paramValue);
}

SendableHttpRequest* SendableHttpRequest::addCookie(const char* cookieName, const char* cookieValue) {
  return (SendableHttpRequest*) HttpRequest::addCookie(cookieName, cookieValue);
}

bool SendableHttpRequest::hasSent() {
  return sent;
}

//Generates key value parameters for querying
void SendableHttpRequest::generateStringList(StringList* stringList, GenericList<HttpParameter>* params, char* separator) {
  ListIterator<HttpParameter> iter = ListIterator<HttpParameter>(params);
  HttpParameter* param = (HttpParameter*) iter.getNext();

  //First parameter
  if(param != NULL) {
    stringList->insertString(param->getParamName());
    stringList->insertString("=");
    stringList->insertString(param->getParamValue());
    param = (HttpParameter*) iter.getNext();
  }

  //Following parameters
  while(param != NULL) {
    stringList->insertString(separator);
    stringList->insertString(param->getParamName());
    stringList->insertString("=");
    stringList->insertString(param->getParamValue());
    param = (HttpParameter*) iter.getNext();
  }
}

//Generates and sends request header and body
void SendableHttpRequest::sendRequest() {
  //Type of request (e.g. POST, GET), resource location
  cl->print(getRequestTypeStr(getType()));
  cl->print(" /");
  cl->print(getController());
  cl->print("/");
  cl->print(getAction());

  //Include host
  cl->println(" HTTP/1.1");
  cl->println(HOST_FIELD);
  cl->println("Accept: */*");
  cl->println("Content-Type: application/x-www-form-urlencoded");
  cl->println("User-Agent: FC Device Client");

  Serial.println("Sending: ");
  Serial.print(getRequestTypeStr(getType()));
  Serial.print(" /");
  Serial.print(getController());
  Serial.print("/");
  Serial.print(getAction());
  Serial.println(" HTTP/1.1");
  Serial.println(HOST_FIELD);
  Serial.println("Accept: */*");
  Serial.println("Content-Type: application/x-www-form-urlencoded");
  Serial.println("User-Agent: FC Device Client");
  
  //Include cookies to pass
  StringList cookieList;

  generateStringList(&cookieList, getCookies(), "; ");

  cl->print("Cookie: ");
  cl->println(cookieList.toString());

  Serial.print("Cookie: ");
  Serial.println(cookieList.toString());

  //Includes parameters within body
  StringList paramList;

  generateStringList(&paramList, getParams(), "&");

  //Include length of body
  int contentLength = paramList.getCharCount();
  cl->print("Content-Length: ");
  cl->println(contentLength);
  cl->println();
  cl->println(paramList.toString());

  Serial.print("Content-Length: ");
  Serial.println(contentLength);
  Serial.println();
  Serial.println(paramList.toString());

  sent = true;
}

//class HttpStandardRequest
HttpStandardRequest::HttpStandardRequest(int type, char* controller, char* action, ResponseHandler handler, WiFiClient* cl)
    : SendableHttpRequest(type, controller, action, cl) {

  this->handler = handler;
}

HttpStandardRequest::~HttpStandardRequest() {
  
}

void HttpStandardRequest::handleResponse(char* responseStr) {
  //Parse response
  HttpResponse response = HttpResponse(responseStr);
  
  //Pass parsed response to handler
  handler(&response);
}

//class HttpAuthenticationRequest
HttpAuthenticationRequest::HttpAuthenticationRequest(int type, char* controller, char* action, AuthenticationResponseHandler handler, WiFiClient* cl)
    : SendableHttpRequest(type, controller, action, cl) {

  this->handler = handler;
}

HttpAuthenticationRequest::~HttpAuthenticationRequest() {
  
}

void HttpAuthenticationRequest::handleResponse(char* responseStr) {
  //Parse authentication response
  HttpAuthenticationResponse response = HttpAuthenticationResponse(responseStr);

  //Pass parsed response to handler
  handler(&response);
}

//As described from https://www.arduino.cc/en/Reference/WiFiStatus
String getWiFiStatusDesc(int s) {
  switch(s) {
    case WL_CONNECTED:
      return "Connected to network";
    case WL_NO_SHIELD:
      return "No WiFi shield present";
    case WL_IDLE_STATUS:
      return "Idle";
    case WL_NO_SSID_AVAIL:
      return "No SSID available";
    case WL_SCAN_COMPLETED:
      return "Scan completed";
    case WL_CONNECT_FAILED:
      return "Connection failed";
    case WL_CONNECTION_LOST:
      return "Lost connection";
    case WL_DISCONNECTED:
      return "Disconnected from network";
    default:
      return "Invalid status";
  }
}

static void gattcCallback(esp_gattc_cb_event_t event,
    esp_gatt_if_t interface, esp_ble_gattc_cb_param_t *param) {

  //For profile registration
  if(event == ESP_GATTC_REG_EVT) {
    Serial.println("Registering");
    if(param->reg.status == ESP_GATT_OK) {
      profiles[param->reg.app_id].interface = interface;
    }
  }

  //Find which profile to callback on
  for(int i = 0; i < NUM_OF_PROFILES; i++) {
    GattcProfile* profile = &profiles[i];

    //If the profile has an interface and it matches
    if(interface == ESP_GATT_IF_NONE ||
        interface == profile->interface) {

      //If there is a callback, call it
      if(profile->callback) {
        Serial.println("Profile callback");
        profile->callback(event, interface, param);
      }
    }
  }
}

//Callback for profile
static void profileHandler(esp_gattc_cb_event_t event,
    esp_gatt_if_t interface, esp_ble_gattc_cb_param_t* param) {

  Serial.print("Event: ");
  Serial.println((int) event);

  switch (event) {
    //Once registered, commence scan for Bluetooth devices
    case ESP_GATTC_REG_EVT: {
      Serial.println("Setting scan parameters");
      esp_err_t err = esp_ble_gap_set_scan_params(&scanParams);
      
      /*if(err) {
        Serial.println("Error, couldn't set scan params");
      }*/
      break;
    }

    //Establish Bluetooth connection
    case ESP_GATTC_CONNECT_EVT: {
      Serial.println("Connect");

      //Copy hex address (as string)
      char* hexAddress = (char*) malloc(sizeof(char)*HEX_ADDR_STR_LEN);
      convertToHex((char*) param->connect.remote_bda, HEX_ADDR_STR_LEN, hexAddress);

      //Get associated WIFI connection
      WiFiConnection* conn = bluetoothWiFiConnections[param->connect.remote_bda];

      //Set the Bluetooth connection ID
      conn->setBTConnId(param->connect.conn_id);

      //Create Bluetooth connection
      Device* device = profiles[APP_ID].devices[hexAddress];
      free(hexAddress);
      uint16_t connectionId = param->connect.conn_id;

      //Connection referred using its id
      connections[connectionId] = new Connection(device, connectionId);

      //For custom mtu
      /*esp_err_t err = esp_ble_gattc_send_mtu_req(interface, *connId);

      if(err) {
        Serial.print("Error, MTU request failed, error code: ");
        Serial.println(err);
      }*/
      break;
    }

    //Open Bluetooth connection
    case ESP_GATTC_OPEN_EVT:
      //If the connection was successful, search for specific service
      if(param->open.status == ESP_GATT_OK) {
        esp_ble_gattc_search_service(interface, param->open.conn_id, &serviceFilter);
      } else {
        //Otherwise, report error and retry
        Serial.print("Couldn't open connection, status: ");
        Serial.println(param->open.status);
        Serial.println("Retrying to open");
        esp_ble_gattc_open(profiles[APP_ID].interface, param->open.remote_bda, BLE_ADDR_TYPE_PUBLIC, true);
      }
      break;

    //When a service is found, note its start and end handles
    case ESP_GATTC_SEARCH_RES_EVT: {
      char hexUUID[HEX_UUID_STR_LEN];
      convertToHex((char*) param->search_res.srvc_id.uuid.uuid.uuid128, HEX_UUID_STR_LEN, hexUUID);

      //Note start and end handles
      Connection* connection = connections[param->search_res.conn_id];
      connection->setStartHandle(param->search_res.start_handle);
      connection->setEndHandle(param->search_res.end_handle);

      //Report discovered service
      Serial.print("Found Service: ");
      Serial.println(hexUUID);
      break;
    }

    //Once the search for services is finished, start finding the specific characteristic
    case ESP_GATTC_SEARCH_CMPL_EVT: {
      //If the search was successful, start finding the characteristics
      if(param->search_cmpl.status == ESP_GATT_OK) {
        uint16_t connId = param->search_cmpl.conn_id;
        Connection* connection = connections[connId];
        uint16_t startHandle = connection->getStartHandle();
        uint16_t endHandle = connection->getEndHandle();
        Device* device = connection->getDevice();

        //Initiate characteristic filter
        esp_bt_uuid_t charaFilter = {
          .len = ESP_UUID_LEN_128,
          .uuid = {.uuid128 = BLANK_UUID}
        };

        //Find each of the characteristics of the device
        for(int i = 0; i < device->getNumOfCharacteristics(); i++) {
          //Get requested characteristic
          uint16_t count = 1;
          esp_gattc_char_elem_t charaElem;
          Characteristic* chara = device->getCharacteristics()[i];

          //Set filter to find specific characteristic
          setCharaFilter(&charaFilter, chara->getUUID());

          //Find characteristic
          esp_gatt_status_t status = esp_ble_gattc_get_char_by_uuid(interface, connId,
            startHandle, endHandle, charaFilter, &charaElem, &count);
          
          //If successfully found characteristic
          if(status == ESP_GATT_OK) {
            //Don't need to check if we can assume all can notify
            if(charaElem.properties&ESP_GATT_CHAR_PROP_BIT_NOTIFY) {
              //If the specific characteristic can notify the hub of change,
              //add a reference to it using its handle as the index
              charaHandles[charaElem.char_handle] = chara;
              
              //Register for notifications from characteristic
              esp_ble_gattc_register_for_notify(interface, connection->getDevice()->getAddress(), charaElem.char_handle);
            }
          } else {
            //If unsuccessful, report characteristic that couldn't be found
            Serial.print("Couldn't get characteristic ");
            Serial.print(i);
            Serial.print(", status: ");
            Serial.println(status);
          }
        }
      } else {
        //If the search for services was unsuccessful, report this
        Serial.print("Couldn't successfully complete search, status: ");
        Serial.println(param->search_cmpl.status);
      }
      break;
    }

    //If received notification, update stored characteristic value
    case ESP_GATTC_NOTIFY_EVT: {
      //Get referenced characteristic from the given characteristic handle
      Characteristic* chara = charaHandles[param->notify.handle];
      
      //Get value and its length
      uint8_t* value = param->notify.value;
      uint16_t valueLength = param->notify.value_len;

      //Update stored value
      chara->setValue(value, valueLength);
      break;
    }
  }
}

static void gapCallback(esp_gap_ble_cb_event_t event,
    esp_ble_gap_cb_param_t* param) {

  switch (event) {
    //After successfully setting scan parameters
    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
      Serial.println("Scan parameters set");
      break;
    }

    //After receiving a scan result
    case ESP_GAP_BLE_SCAN_RESULT_EVT: {
      Serial.println("Got scan result");
      handleScanResult(param);
      break;
    }
  }
}

//Handles a given scan result
void handleScanResult(esp_ble_gap_cb_param_t* scanResult) {
  switch (scanResult->scan_rst.search_evt) {
    //When a new device is found
    case ESP_GAP_SEARCH_INQ_RES_EVT:
      //Pass the device's details and Bluetooth address for parsing
      Device* device = Device::createDevice(scanResult->scan_rst.ble_adv,
        scanResult->scan_rst.bda);

      Serial.print("Raw Address: ");
      Serial.println((char*) scanResult->scan_rst.bda);
      
      if(device) {
        Serial.println("Found BLE Device:");
        Serial.print("Name: ");
        Serial.println(device->getName());
        Serial.print("Model Number: ");
        Serial.println(device->getModelNumber());
        Serial.print("Serial Number: ");
        Serial.println(device->getSerialNumber());
        Serial.print("Address: ");
        Serial.println(device->getHexAddress());
        Serial.print("Address Type: ");
        Serial.println((int) scanResult->scan_rst.ble_addr_type);

        //Check if device is already stored, if it isn't, relay search result
        Device* existingDevice = (*scannedDevices)[scanResult->scan_rst.bda];

        if(existingDevice == NULL) {
          //Relay the name of the device, model number, serial number
          //and Bluetooth address
          scanClient->println(device->getName());
          scanClient->println(device->getModelNumber());
          scanClient->println(device->getSerialNumber());
          scanClient->println(device->getHexAddress());

          //Store device
          (*scannedDevices)[scanResult->scan_rst.bda] = device;
        }
      }
      
      break;
  }
}

void initBluetooth() {
  Serial.println("Init Bluetooth");
  //Init non-volatile storage
  esp_err_t err = nvs_flash_init();

  if(err) {
    Serial.println("Error, couldn't init nvs flash, error code: ");
  }

  //Start Bluetooth
  btStart();

  err = esp_bluedroid_init();
  if(err) {
    Serial.print("Error, couldn't init bluetooth, error code: ");
    Serial.println(err);
  }

  err = esp_bluedroid_enable();
  if(err) {
    Serial.print("Error, couldn't enable bluetooth, error code: ");
    Serial.println(err);
  }

  //Register generic access profile callback
  err = esp_ble_gap_register_callback(gapCallback);
  if(err) {
    Serial.print("Error, couldn't register gap callback, error code: ");
    Serial.println(err);
  }

  //Register generic attribute profile callback
  err = esp_ble_gattc_register_callback(gattcCallback);
  if(err) {
    Serial.print("Error, couldn't register gattc callback, error code: ");
    Serial.println(err);
  }

  //Register application
  err = esp_ble_gattc_app_register(APP_ID);
  if(err) {
    Serial.print("Error, couldn't register app, error code: ");
    Serial.println(err);
  }
}

//Remove closed WiFi connections
void handleWiFiConnections() {
  int numOfConnections = wifiConnections.getCount();
  GenericElem<WiFiConnection>* current = wifiConnections.getHead();

  //Find closed WiFi connections
  for(int i = 0; i < numOfConnections; i++) {
    WiFiConnection* wifiConnection = current->getValue();
    WiFiClient* cl = wifiConnection->getWiFiClient();

    GenericElem<WiFiConnection>* prev = current;
    current = current->getNext();

    //Check if closed
    if(!cl->connected()) {
      //Handle the closing of the connection and remove
      //from the list of connections
      wifiConnection->handleClose();
      wifiConnections.removeElem(prev);
      Serial.println("Request finished");
    }
  }
}

//Web method: Handle request to relay stream of sensor data
void sensorData(WiFiConnection* conn) {
  WiFiClient* cl = conn->getWiFiClient();
  Parameters* params = conn->getParams();
  char* hexAddress = (*params)["Address"];

  //Find the requested device to obtain data from
  std::unordered_map<char*, Device*, StringHasher, StringComparator>* devices =
    &(profiles[APP_ID].devices);

  Device* device;

  //If the device being referred doesn't exist, create it
  if(devices->find(hexAddress) == devices->end()) {
    //Create device, provide Bluetooth address and the UUID
    //of the characteristic
    char* charaUUIDsStr = (*params)["CharaUUIDS"];
    device = Device::createDevice(hexAddress, charaUUIDsStr);

    //Store device
    (*devices)[device->getHexAddress()] = device;
  } else {
    //Get device
    device = (*devices)[hexAddress];
  }

  //Start header
  cl->println("HTTP/1.1 200 OK");
  cl->println("Content-Type: application/json");
  cl->println();

  //Associate with the client WiFi connection
  device->setWiFiClient(cl);

  //Copy address (in bytes)
  uint8_t* address = (uint8_t*) malloc(sizeof(uint8_t)*ESP_BD_ADDR_LEN);

  convertHexToBytes(hexAddress, address, HEX_ADDR_LEN);

  Serial.print("SensorData: Hex Address: ");
  Serial.println(hexAddress);

  Serial.print("SensorData: Address: ");
  Serial.println((char*) address);

  //Associate Bluetooth connection with WiFi connection
  bluetoothWiFiConnections[address] = conn;

  //Open Bluetooth connection
  esp_ble_gattc_open(profiles[APP_ID].interface, address,
    BLE_ADDR_TYPE_PUBLIC, true);
}

//Web method: Close connection for relaying stream of sensor data
void closeSensorData(Parameters* params) {
  //Get Bluetooth address in hex (as string)
  char* hexAddress = (*params)["Address"];
  uint8_t address[ESP_BD_ADDR_LEN];

  //Get Bluetooth address in bytes
  convertHexToBytes(hexAddress, address, HEX_ADDR_LEN);

  //Get WiFi connection associated with Bluetooth address
  WiFiConnection* conn = bluetoothWiFiConnections[address];

  //Close associated Bluetooth connection
  esp_ble_gattc_close(profiles[APP_ID].interface, conn->getBTConnId());
}

//Web method: Handle request to scan for Bluetooth beacons
void scanBeacons(WiFiConnection* conn) {
  WiFiClient* cl = conn->getWiFiClient();

  //Start header
  cl->println("HTTP/1.1 200 OK");
  cl->println("Content-Type: application/json");
  cl->println();

  //Store client connection that is requesting for the scan
  scanClient = cl;

  //Reset set of scanned devices
  scannedDevices = new std::unordered_map<uint8_t*, Device*, AddressHasher, AddressComparator>();

  //Commence scanning (scanned devices handled within callback)
  Serial.println("Start Scanning");
  esp_ble_gap_start_scanning(SCAN_DURATION);

  //Delay until scan complete (done in a fixed amount of time)
  delay(SCAN_DURATION*1000);

  //Stop scanning
  esp_ble_gap_stop_scanning();

  Serial.println("COMPLETE");

  delete scannedDevices;

  //Need to reset scan parameters before the start of a
  //potential next scan
  Serial.println("Resetting scan parameters");
  esp_err_t err = esp_ble_gap_set_scan_params(&scanParams);

  //Close connection afterwards
  cl->stop();
}

//Web method: Close connection for relaying scanned Bluetooth beacons
void closeScanBeacons(Parameters* params) {
  
}

//Registers web methods
void initWebMethodsMapping() {
  //The key is named after the action name and the value
  //is given as the reference to the method associated
  //with the action that shall handle the given request
  
  webMethodsMap["ScanBeacons"] = &scanBeaconsWebMethod;
  webMethodsMap["SensorData"] = &sensorDataWebMethod;
}

//Handle the given client (that has a request)
void handleRequest(WiFiClient* webClient) {
  //Parse request
  HttpRequest* request = HttpRequest::parseRequest(webClient);
  
  Parameters* parameters = new Parameters();

  //Get query parameters of the request
  GenericList<HttpParameter>* queryParams = request->getQueryParams();

  //Store each parameter
  GenericElem<HttpParameter>* current = queryParams->getHead();

  for(int i = 0; i < queryParams->getCount(); i++) {
    HttpParameter* param = current->getValue();

    //Get parameter name and value
    char* paramName = param->getParamName();
    char* paramValue = param->getParamValue();
    int paramNameLen = strlen(paramName)+1;
    int paramValueLen = strlen(paramValue)+1;

    //Copy name and value and store
    char* paramNameCopy = (char*) malloc(paramNameLen*sizeof(char));
    char* paramValueCopy = (char*) malloc(paramValueLen*sizeof(char));
    memcpy(paramNameCopy, paramName, paramNameLen);
    memcpy(paramValueCopy, paramValue, paramValueLen);
    (*parameters)[paramNameCopy] = paramValueCopy;
    current = current->getNext();
  }

  //Get the web method associated with the action given from
  //the request
  char* action = request->getAction();
  WebMethod* webMethod = webMethodsMap[action];

  //Check if the associated web method exists so the request
  //can be handled
  if(webMethod == NULL) {
    //TODO: Send bad request here
    webClient->stop();
  } else {
    //Create the WiFi connection and store the client connection,
    //called web method and the query parameters
    WiFiConnection* wifiConnection = new WiFiConnection(webClient, webMethod, parameters);

    //Include the WiFi connection onto the list of connections
    wifiConnections.insertDestroyable(wifiConnection);

    //Call the web method to handle the request
    webMethod->handleRequest(wifiConnection);
  }
}

//Called at the very start of the program
void setup() {
  Serial.begin(BAUD_RATE);

  //Setup update timer - used for frequently relaying characteristic data
  updateTimer = timerBegin(CLOCK_ID, CLOCK_PRESCALER, CLOCK_COUNT_UP);
  
  //Set method for timer to frequently call
  timerAttachInterrupt(updateTimer, &onUpdate, CLOCK_EDGE_TRIGGER);

  //Set time between calls
  timerAlarmWrite(updateTimer, CLOCK_TRIGGER_MICROSECONDS, CLOCK_LOOP);
  timerAlarmEnable(updateTimer);

  //Register web methods
  initWebMethodsMapping();

  //Initiate Bluetooth
  initBluetooth();

  //Use predefined ssid and password to connect WiFi
  int result = WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  //Timeout if the time to connect takes too long,
  //print dots to mark time passed
  int timeoutCounter = 0;
  while (WiFi.status() != WL_CONNECTED && timeoutCounter < CONNECT_TIMEOUT_COUNT) {
    delay(CHECK_WIFI_STATUS_DELAY_MILLISEC);
    Serial.print(".");
    timeoutCounter++;
  }

  Serial.println();

  if(WiFi.status() == WL_CONNECTED) {
    //If successfully connected, start server
    wifiServer.begin();
    
    Serial.print("Successfully connected to ");
    Serial.println(WIFI_SSID);
    
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    //First, try to connect to the web server
    if(wifiClient.connect(SERVER, PORT)) {
      //Send request to attempt to register self (logs in otherwise)
      HttpAuthenticationRequest* request = new HttpAuthenticationRequest(
        HttpRequest::POST, "Account", "AddClientDevice",
        &handleRegisterResponse, &wifiClient);

      //Include username and password for registration
      request->addParam("username", "test")->addParam("password", "MyPassword%26123");

      //Add request to queue
      httpRequestsQueue.insertDestroyable(request);
    }
  } else {
    //If connection was unsuccessful, report the issue
    Serial.print("Timeout, couldn't connect after ");
    Serial.print(CONNECT_TIMEOUT_MILLISEC);
    Serial.println(" milliseconds.");
    
    Serial.print("Status: ");
    Serial.println(getWiFiStatusDesc(result));
  }

  Serial.println("Setup Finished");
}

bool disconnected = false;

//Frequently called (after setup is called) while the program is running
void loop() {
  //Get the next request from the queue
  SendableHttpRequest* request = (SendableHttpRequest*) httpRequestsQueue.getHeadValue();

  //If there is a request and it hasn't been sent, send it,
  //check if there is a response for that request and remove
  //the request once the response has been received and handled
  if(request != NULL) {
    //Send request if it hasn't been sent yet
    if(!request->hasSent()) {
      request->sendRequest();
    }

    //If there is an incoming response, handle it
    if(wifiClient.available()) {
      CharList list;

      //Retrive entire response
      while(wifiClient.available()) {
        char* c = new char(wifiClient.read());
        list.insertDestroyable(c);
      }
      
      char* str = list.toString();
      Serial.println(str);

      //Handle response
      request->handleResponse(str);
      httpRequestsQueue.removeHead();
      
      free(str);
    }
  }

  //Check for incoming requests
  WiFiClient webClient = wifiServer.available();

  //If there is an incoming request, handle it
  if(webClient) {
    WiFiClient* dynWebClient = new WiFiClient(webClient);
    handleRequest(dynWebClient);
  }

  //Check if the hub has disconnected from the server
  if(!disconnected && !wifiClient.connected()) {
    Serial.println();
    Serial.println("Disconnected from server");
    disconnected = true;
  }

  //Check if it is time to relay the values of the characteristics
  if(updateDevicesTime) {
    Serial.println("Updating devices");

    //Relay the values of the characteristics of each device
    int numOfDevices = updateDevices.getCount();
    for(int i = 0; i < numOfDevices; i++) {
      Device* device = updateDevices.getHeadValue();
      device->update();
      updateDevices.removeHead();
    }
    updateDevicesTime = false;
  }

  handleWiFiConnections();
}
